 
Source: [File:Communities of Belgium.svg](https://en.wikipedia.org/wiki/File:Communities_of_Belgium.svg) on Wikipedia.

Licences:

* GNU Free Documentation License, Version 1.2 or any later version published by the Free Software Foundation.
* Creative Commons Attribution-Share Alike 3.0 Unported license. 

Author: [Ssolbergj](https://en.wikipedia.org/wiki/User:Ssolbergj).
