// create-checkbox-with-label-web-component.js

const template = document.createElement("template");
template.innerHTML = `
  <style>
    label { color: #008B8B;
      display: block;
    }
    .description { font-size: 0.85rem;
      color: #777;
    }
  </style>

  <label>
    <input type="checkbox" />
    <slot></slot>
    <span class="description">
      <slot name="description"></slot>
    </span>
  </label>
`;

class TodoItem extends HTMLElement {
  constructor() {
    super();
    console.log('— this', this);
    const shadow = this.attachShadow({ mode: "open"});
    shadow.append(template.content.cloneNode(true));
    this.checkbox = shadow.querySelector("input");
    /*
    const shadowRoot = this.attachShadow( {
        mode: 'closed',
        delegatesFocus: true // not doing anything in Firefox
    })
    */
    /*
    shadowRoot.innerHTML = `
      <style>
        label { font-weight: bold;
          font-face: 'Comic Sans';
          background-color: yellow;
        }
      </style>
      <input class="custum-checkbox" type="checkbox" id="${ this.id }" name="${ this.name }" />
      <label for="${ this.id }">this.name = ${ this.name }</label>
    `;
    */
  }
  attributeChangedCallback(name, oldValue, newValue) {
    console.log('— attribute changed', name, oldValue, newValue, this.hasAttribute(name));
    if ( name === "checked") this.updateChecked(newValue);
  }
  static  get observedAttributes() {
    return ['checked']; // list of attributes to observe
  }

  connectedCallback() {
    console.log('— mounted');
  }

  disconnectedCallback() {
    console.log('— unmounted');
  }

  updateChecked(value) {
    this.checkbox.checked = value != null && value !== "false"
  }
  /*
  adoptedCallback() {
    console.log('— adopted');
  }
  */

}

customElements.define( 'todo-item', TodoItem); // element name (with hyphen), class

/*
const item = document.querySelector("todo-item");
let checked = "true";
setInterval(() => {
    checked =  !checked;
    item.setAttribute("checked", checked)
}, 10000);
*/
