/*form-elements.js*/
/*jslint
    this
    for
*/

let DEBUG = false;

/**
 * @namespace The main application object.
 */
let formsdemo = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addJumpMenuListener();
  },

  addJumpMenuListener:function() {
    "use strict";
    addListener(document.getElementById("jump-menu-after"), "submit", formsdemo.handleChoiceToggle);
  },

  handleChoiceToggle:function(event) {
    "use strict";
    // @@TODO replace with DOMhelp.stopDefault(event) and DOMhelp.stopBubble(event) (?)
    event.preventDefault();
    event.stopPropagation();
    let selectList = event.target.querySelector("select");
    let newPage = selectList.options[selectList.selectedIndex].value;
    window.location = newPage;
  }
};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    formsdemo.init();
});
