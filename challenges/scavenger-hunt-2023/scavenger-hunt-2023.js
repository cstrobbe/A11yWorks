/*scavenger-hunt-2023.js*/
/*jslint
    this
    for
*/

let DEBUG = true;

/**
 * @namespace The main application object.
 */
let theHunt = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addUnblurListener();
    this.addUninvertListener();
    this.addUngrayscaleListener();
  },

  addUnblurListener:function() {
    "use strict";
    addListener(document.getElementById("unblur"), "click", theHunt.handleBlurToggle);
  },

  addUninvertListener:function() {
    "use strict";
    addListener(document.getElementById("uninvert"), "click", theHunt.handleInvertToggle);
  },

  addUngrayscaleListener:function() {
    "use strict";
    addListener(document.getElementById("ungrayscale"), "click", theHunt.handleGrayscaleToggle);
  },

  handleBlurToggle:function(event) {
    "use strict";
    event.preventDefault();
    event.stopPropagation();
    theHunt.toggleBlur(event.target, event.type);
  },

  handleInvertToggle:function(event) {
    "use strict";
    event.preventDefault();
    event.stopPropagation();
    theHunt.toggleInvert(event.target, event.type);
  },

  handleGrayscaleToggle:function(event) {
    "use strict";
    event.preventDefault();
    event.stopPropagation();
    theHunt.toggleGrayscale(event.target, event.type);
  },

  toggleBlur:function(target, type) {
    "use strict";
    let theElement = document.getElementById("blurry");
    if (DEBUG) { console.log("typeOf theElement: " + typeOfVar(theElement)); }
    if (DEBUG) { console.log("toggleBlur. classList = " + theElement.classList); }

    if (theElement.classList.contains("blurry")) {
      theElement.classList.remove("blurry");
    } else {
      theElement.classList.add("blurry");
    }
  },

  toggleInvert:function(target, type) {
    "use strict";
    let theName = "invert-colours";
    let theElement = document.getElementById(theName);
    if (DEBUG) { console.log("typeOf theElement: " + typeOfVar(theElement)); }
    if (DEBUG) { console.log("toggleInvert. classList = " + theElement.classList); }

    if (theElement.classList.contains(theName)) {
      theElement.classList.remove(theName);
    } else {
      theElement.classList.add(theName);
    }
  },

  toggleGrayscale:function(target, type) {
    "use strict";
    let theName = "grayscale";
    let theElement = document.getElementById(theName);
    if (DEBUG) { console.log("typeOf theElement: " + typeOfVar(theElement)); }
    if (DEBUG) { console.log("toggleGrayscale. classList = " + theElement.classList); }

    if (theElement.classList.contains(theName)) {
      theElement.classList.remove(theName);
    } else {
      theElement.classList.add(theName);
    }
  }
};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    theHunt.init();
});


 
