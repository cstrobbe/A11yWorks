/*spinner-04.js*/
/*jslint
    this
    for
*/

let DEBUG = false;

/**
 * @namespace The main application object.
 */
let spinner = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addSpinnerButtonListener();
  },

  addSpinnerButtonListener:function() {
    "use strict";
    const theButton = document.getElementById("add-alert")
    theButton.addEventListener("click", spinner.handleSpinnerDisplay);
    //console.log("add-alert is " + theButton.nodeName);
  },

  handleSpinnerDisplay:function(event) {
    "use strict";
    // @@TODO replace with DOMhelp.stopDefault(event) and DOMhelp.stopBubble(event) (?)
    event.preventDefault();
    event.stopPropagation();
    spinner.displaySpinner(event.target, event.type);
  },

  displaySpinner:function(target, type) {
    "use strict";
    const spinnerMessageID = "spinner-messaage";
    const button = document.getElementById("add-alert");
    const spinnerWidget = document.createElement("div");
    spinnerWidget.setAttribute("role", "alert");
    spinnerWidget.setAttribute("class", "lds-dual-ring");
    //spinnerWidget.setAttribute("aria-describedby", spinnerMessageID);
    // Using role=alert makes sure NVDA announces the spinner; using aria-live without role is pointless.
    //spinnerWidget.setAttribute("aria-live", "assertive");
    const spinnerMessage = document.createElement("span");
    spinnerMessage.setAttribute("class", "sr-only");
    spinnerMessage.setAttribute("id", spinnerMessageID);
    spinnerMessage.appendChild(document.createTextNode("Loading"));
    spinnerWidget.appendChild(spinnerMessage);
    button.insertAdjacentElement("afterend", spinnerWidget);
  }
};

addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    spinner.init();
});

/**
 * Add a function to the list of functions that need to be called when
 * the HTML document has finished loading (in other words, after the <code>window.load</code> event).
 * @param {Function} func A function that needs to be invoked after <code>window.load</code>.
 * @static
 * @author Simon Willison
 * @see Simon Willison's article <a href="https://simonwillison.net/2004/May/26/addLoadEvent/">Executing JavaScript on page load</a> (26 May 2004; last acccessed on 09.08.2022).
 */
function addLoadEvent(func) {
    "use strict";
    var oldonload = window.onload;
    if (typeof window.onload !== 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        };
    }
}
