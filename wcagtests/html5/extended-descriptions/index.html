<!DOCTYPE html>
<html lang="en-GB">
<head>
  <meta charset="utf-8" />
  <title>Test Pages for Extended Descriptions | Accessibility Works</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=yes" />
  <link rel="stylesheet" type="text/css" media="screen" href="../../../css/verdemoderna.css" title="default" />
  <link rel="top" href="../../../index.html" title="Accessibility Works: Home page" />
  <meta name="robots" content="noindex, nofollow" />
</head>
<body>
  <header id="header">
    <h1><span class="h1txt">Test Pages for Extended Descriptions</span></h1>
  </header>

  <main id="main">
    <h2>Test Pages</h2>
    <p>These pages are for testing support for the attributes <code>aria-details</code> and <code>aria-describedby</code>
      as a means of providing extended descriptions of charts and videos.
    </p>
    <ul>
      <li><a href="pie-chart-aria-details.html">Pie chart with <code>aria-details</code></a>.</li>
      <li><a href="pie-chart-aria-describedby.html">Pie chart with <code>aria-describedby</code></a>.</li>
      <li><a href="video-native-stallman-aria-details.html">Video (native player) with <code>aria-details</code></a>.</li>
      <li><a href="video-native-stallman-aria-describedby.html">Video (native player) with <code>aria-describedby</code></a>.</li>
      <li><a href="video-native-stallman.html">Test Page for Native Video Player with <code>title</code> Attribute</a> (no extended description).</li>
      <li><a href="video-youtube-embed-stallman.html">Test Page for YouTube Video Player with <code>title</code> Attribute</a> (no extended description).</li>
      <li><a href="video-youtube-embed-stallman-aria-describedby.html">The same YouTube video with <code>aria-describedby</code></a>.</li>
      <li><a href="video-youtube-embed-stallman-aria-details.html">The same YouTube video with <code>aria-details</code></a>.</li>
      <!--@@todo add to sitemap and index.html-->
      <li><a href="map-osm-aria-details.html">Test Page for Embedded OpenStreetMap with <code>aria-details</code> Attribute</a>.
        (There are other methods of embedding OpenStreetMap maps than the one tested here. See, for example,
        <a href="https://gis.stackexchange.com/questions/341510/how-to-use-openstreetmap-in-webpage-via-osm-url-without-leaflet-openlayers-etc">How to use OpenStreetMap in webpage via <abbr>OSM</abbr> <abbr>URL</abbr> without Leaflet, OpenLayers, etc?</a>
        and <a href="https://ircama.github.io/osm-carto-tutorials/map-client/">JavaScript libraries for implementing <abbr>OSM</abbr> interactive maps</a>.)
        <!--see also
          How to include OpenStreetMap in my Web app: https://stackoverflow.com/questions/13937671/how-to-include-openstreetmap-in-my-web-app
        -->
      </li>
      <li><a href="map-osm-aria-describedby.html">Test Page for Embedded OpenStreetMap with <code>aria-describedby</code> Attribute</a>.</li>
      <li><a href="map-svg-inline-aria-details.html">Test Page for Inline <abbr>SVG</abbr> Map with <code>aria-details</code> Attribute</a>.</li>
      <li><a href="map-svg-inline-aria-describedby.html">Test Page for Inline <abbr>SVG</abbr> Map with <code>aria-describedby</code> Attribute</a>.</li>
      <!--<li><a href="">Test Page for <abbr>SVG</abbr> Map in <abbr>img</abbr> Element</a>.</li>-->
      <!--
      <li><a href=""></a>.</li>
      -->
    </ul>

    <h2>Background</h2>
    <p>When using <a href="https://www.w3.org/TR/wai-aria-1.1/#aria-details"><code>aria-details</code></a>, the attribute <em>should</em> refer to an element that is visible to all users.
      When using <a href="https://www.w3.org/TR/wai-aria-1.1/#aria-describedby"><code>aria-describedby</code></a>, by contrast, there is no requirement related to the visibility of the referenced element.
    </p>
    <p>Another important difference between <code>aria-details</code> and <code>aria-describedby</code> is the following:</p>
    <blockquote>
      <p>Unlike elements referenced by <code>aria-describedby</code>, the element referenced by <code>aria-details</code> is not used in either the Accessible <a href="https://www.w3.org/TR/accname-aam-1.1/#mapping_additional_nd_name">Name Computation</a> or
        the Accessible <a href="https://www.w3.org/TR/accname-aam-1.1/#mapping_additional_nd_description">Description Computation</a> as defined in the Accessible Name and Description specification [<cite><a class="bibref" href="https://www.w3.org/TR/accname-1.1/">accname-aam-1.1</a></cite>].
        Thus, the content of an element referenced by <code>aria-details</code> is not flattened to a string when presented to assistive technology users.
        This makes <code>aria-details</code> particularly useful when converting the information to a string would cause a loss of information or make the extended description more difficult to understand.
      </p>
    </blockquote>
    <p>Other resources:</p>
    <ul>
      <li><a href="https://a11ysupport.io/tech/aria/aria-details_attribute"><code>aria-details</code> (support)</a>, <i class="website">Accessibility Support</i>, accessed on 01.03.2023.
        <br />According to this page, <code>aria-details</code> is not supported by TalkBack and VoiceOver and only partially supported in the desktop.
        However, the presence of <code>aria-details</code> is conveyed in the major screen reader and browser combinations (except Narrator with Edge).
      </li>
      <li><a href="https://a11ysupport.io/tech/aria/aria-description_attribute"><code>aria-description</code> (support)</a>, <i class="website">Accessibility Support</i>, accessed on 05.03.2023.
        <br />The attribute <code>aria-description</code> was introduced in
        <a href="https://w3c.github.io/aria/#aria-description">working drafts of <abbr>WAI-ARIA</abbr> 1.3</a>
        (added in January 2020; see <a href="https://w3c.github.io/aria/#substantive-changes-targeted-for-the-1-3-release">Substantive changes targeted for the 1.3 release</a>).
        Like <code>aria-label</code>, the value of <code>aria-description</code> is a flat string.
      </li>
      <li>O'Hara, Scott:
        <a href="https://www.scottohara.me/blog/2018/11/08/aria-details.html">Getting the details on aria-details</a>,
        <i class="blog">scottohara.me</i>, 08.11.2018.
        <br />At the time this blog post was written, only <abbr>JAWS</abbr> supported <code>aria-details</code> to some extent.
        Even when the availability of details was announced, it was not clear how to navigate to them.
      </li>
      <li>Raghavendra Satish Peri:
        <a href="https://www.digitala11y.com/aria-detailsproperties/"><abbr>WAI-ARIA</abbr>: aria-details (Property)</a>,
        <i class="website">DigitalA11Y</i>, 14.01.2023 (accessed on 05.03.2023).
        <br />This page contains an outdated table document support by various combinations of screen readers and browsers.
        For example, the table claims that there is no support at all by <abbr>NVDA</abbr>.
      </li>
      <li>
        <a href="https://github.com/nvaccess/nvda/issues/13940">Navigate to aria-details target and return to initial location</a>:
        <abbr>NVDA</abbr> issue or feature request submitted by George Kerscher 24 July 2022.
        This points out that <kbd>NVDA key +D</kbd> can be used to read to contents of the element referenced by <code>aria-details</code>.
      </li>
    </ul>
  </main>

  <footer id="footer">
    <p id="licence">
      <a class="image" rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
      <img alt="Creative Commons License Attribution-ShareAlike" style="border-width:0" src="../../../img/cc-licence-by-sa-88x31.png" />
      </a>
      <br />
      <a href="https://cstrobbe.gitlab.io/A11yWorks/">Accessibility Works</a>
      by <a href="https://gitlab.com/cstrobbe">Christophe Strobbe</a> is licensed under a
      <a lang="en-US" rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.
    </p>
    <p><a href="../../../sitemap.html">Site map</a>.</p>
    <p>You can <a href="https://gitlab.com/cstrobbe/A11yWorks">fork the code</a> on GitLab.</p>
  </footer>
</body>
</html>