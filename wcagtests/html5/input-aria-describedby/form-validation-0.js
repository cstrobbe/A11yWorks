/*form-validation-0.js*/
/*jslint
    this
    for
*/

let DEBUG = true;

/**
 * @namespace The main application object.
 */
let formValidation = {
  init:function() {
    "use strict";
    this.addMailFieldListener();
    this.addDateFieldListeners();

    this.addTravelDateFieldListeners();

    this.addOnResetListener();
  },

  addOnResetListener:function(event) {
    "use strict";
    document.getElementById('form1').addEventListener('reset', formValidation.handleForm1Reset);
    document.getElementById('form2').addEventListener('reset', formValidation.handleForm2Reset);
  },
  handleForm1Reset:function(event) {
    "use strict";
    formValidation.hideErrorMessages("form1", "error-message");
    formValidation.resetInvalidAttribute("form1");
  },
  handleForm2Reset:function(event) {
    "use strict";
    formValidation.hideErrorMessages("form2", "error-message");
    formValidation.resetInvalidAttribute("form2");
  },
  hideErrorMessages:function(theID, theClass) {
    "use strict";
    let container = document.getElementById(theID);
    /*
    // see https://stackoverflow.com/a/12166811/6002174
    for (var i = 0; i < container.childNodes.length; i++) { // strictly speaking, we need descendants, not necessarily just child nodes
      if ( container.childNodes[i].className === theClass) {
        container.childNodes[i].classList.add("hidden");
      }
    }
    */
    let descendents = container.getElementsByClassName(theClass);
    for (const d of descendents) {
      d.classList.add("hidden");
    }
  },
  resetInvalidAttribute:function(theID) {
    "use strict";
    let container = document.getElementById(theID);
    let descendents = container.getElementsByTagName('input');
    for (const d of descendents) {
      d.removeAttribute("aria-invalid");
    }
  },

  addMailFieldListener:function(event) {
    "use strict";
    document.getElementById('email-1').addEventListener('blur', formValidation.handleEmailValidation);
    if (DEBUG) { console.log("addMailFieldListener() has run."); }
  },

  addDateFieldListeners:function() {
    "use strict";
    document.getElementById('startdate-1').addEventListener('input', formValidation.handleDateValidation);
    document.getElementById('enddate-1').addEventListener('input', formValidation.handleDateValidation);
    if (DEBUG) { console.log("addDateFieldListeners() has run."); }
  },

  addTravelDateFieldListeners:function() {
    "use strict";
    document.getElementById('departure-date').addEventListener('input', formValidation.handleTravelDateValidation);
    document.getElementById('return-date').addEventListener('input', formValidation.handleTravelDateValidation);
    if (DEBUG) { console.log("addTravelDateFieldListeners() has run."); }
  },

  handleEmailValidation:function() {
    "use strict";
    if (DEBUG) { console.log("Start mail validation."); }

    let mailfield = document.getElementById('email-1');
    let mail = mailfield.value.trim();
    let emptyMailWarning = document.getElementById('mail-empty-message');
    let invalidMailWarning = document.getElementById('mail-invalid-message');

    if ( mail === '') {
      emptyMailWarning.classList.remove("hidden");
      invalidMailWarning.classList.add("hidden");
      mailfield.setAttribute("aria-invalid", "true");
      mailfield.setAttribute("aria-describedby", "mail-empty-message"); // not great to hard-code all this stuff, but it's just a demo
    } else if ( !formValidation.isValidEmail(mail)) {
      emptyMailWarning.classList.add("hidden");
      invalidMailWarning.classList.remove("hidden");
      mailfield.setAttribute("aria-invalid", "true");
      mailfield.setAttribute("aria-describedby", "mail-invalid-message"); // not great to hard-code all this stuff, but it's just a demo
    } else {
      emptyMailWarning.classList.add("hidden");
      invalidMailWarning.classList.add("hidden");
      mailfield.removeAttribute("aria-invalid");
      mailfield.removeAttribute("aria-describedby");
    }

  },

  handleDateValidation:function() {
    "use strict";
    let startDateField = document.getElementById("startdate-1");
    let endDateField = document.getElementById("enddate-1");
    let startDate = startDateField.value;
    let endDate = endDateField.value;
    let invalidDateWarningBefore = document.getElementById('start-before-end-date');
    let invalidDateWarningAfter = document.getElementById('end-after-start-date');

    if ( !formValidation.startDateIsBeforeEndDate(startDate, endDate)) {
      if (startDate != "") {
        startDateField.setAttribute("aria-invalid", "true");
        invalidDateWarningBefore.classList.remove("hidden");
        startDateField.setAttribute("aria-describedby", "start-before-end-date");
      }
      if ( endDate != "") {
        endDateField.setAttribute("aria-invalid", "true");
        invalidDateWarningAfter.classList.remove("hidden");
        endDateField.setAttribute("aria-describedby", "end-after-start-date");      
      }
    } else {
      startDateField.removeAttribute("aria-invalid");
      endDateField.removeAttribute("aria-invalid");
      invalidDateWarningBefore.classList.add("hidden");
      invalidDateWarningAfter.classList.add("hidden");
      startDateField.removeAttribute("aria-describedby");
      endDateField.removeAttribute("aria-describedby");      
    }
    if (DEBUG) { console.log("Date validation has run."); }
  },

  handleTravelDateValidation:function() {
    "use strict";
    let departureDateField = document.getElementById("departure-date");
    let returnDateField = document.getElementById("return-date");
    let departureDate = departureDateField.value;
    let returnDate = returnDateField.value;
    let invalidDateWarning = document.getElementById('travel-date-error');

    if ( !formValidation.startDateIsBeforeEndDate(departureDate, returnDate)) {
      if (departureDate != "") {
        departureDateField.setAttribute("aria-invalid", "true");
        invalidDateWarning.classList.remove("hidden");
        departureDateField.setAttribute("aria-describedby", "travel-date-error");
        if (DEBUG) { console.log("Travel departure date issue."); }
      }
      if ( returnDate != "") {
        returnDateField.setAttribute("aria-invalid", "true");
        invalidDateWarning.classList.remove("hidden");
        returnDateField.setAttribute("aria-describedby", "travel-date-error");      
        if (DEBUG) { console.log("Travel return date issue."); }
      }
    } else {
      departureDateField.removeAttribute("aria-invalid");
      returnDateField.removeAttribute("aria-invalid");
      invalidDateWarning.classList.add("hidden");
      departureDateField.removeAttribute("aria-describedby");
      returnDateField.removeAttribute("aria-describedby");      
    }
    
    if (DEBUG) { console.log("Travel date validation has run."); }
  },

  startDateIsBeforeEndDate:function(startDate, endDate) {
    "use strict";
    let datesOK = false;
    if ( startDate < endDate) {
      datesOK = true;
    }
    return datesOK; 
  },

  isValidEmail:function(address) {
    "use strict";
    let isValid = false;
    // regex from https://stackoverflow.com/a/9204568/6002174 - only checks syntax, not whether the address exists, obviously
    let rex = /\S+@\S+\.\S+/;
    isValid = rex.test(address);
    return isValid;
  }
};

formValidation.init();
