/*form-validation-0.js*/
/*jslint
    this
    for
*/

let DEBUG = true;

/**
 * @namespace The main application object.
 */
let formValidation = {
  init:function() {
    "use strict";
    this.addMailFieldListener();
    this.addDateFieldListeners();

    this.addOnResetListener();
  },

  addOnResetListener:function(event) {
    "use strict";
    document.getElementById('form1').addEventListener('reset', formValidation.handleForm1Reset);
  },
  handleForm1Reset:function(event) {
    "use strict";
    formValidation.removeAlertAttribute("form1");
    formValidation.resetInvalidAttribute("form1");
  },
  resetInvalidAttribute:function(theID) {
    "use strict";
    let container = document.getElementById(theID);
    let descendents = container.getElementsByTagName('input');
    for (const d of descendents) {
      d.removeAttribute("aria-invalid");
    }
  },
  removeAlertAttribute:function(theID) {
    "use strict";
    let container = document.getElementById(theID);
    let descendents = container.getElementsByTagName('label');
    for (const d of descendents) {
      d.removeAttribute("role");
    }
  },

  addMailFieldListener:function(event) {
    "use strict";
    document.getElementById('email-1').addEventListener('blur', formValidation.handleEmailValidation);
    if (DEBUG) { console.log("addMailFieldListener() has run."); }
  },

  addDateFieldListeners:function() {
    "use strict";
    document.getElementById('startdate-1').addEventListener('input', formValidation.handleDateValidation);
    document.getElementById('enddate-1').addEventListener('input', formValidation.handleDateValidation);
    document.getElementById('startdate-1').addEventListener('blur', formValidation.handleDateValidation);
    document.getElementById('enddate-1').addEventListener('blur', formValidation.handleDateValidation);
    if (DEBUG) { console.log("addDateFieldListeners() has run."); }
  },

  handleEmailValidation:function() {
    "use strict";
    if (DEBUG) { console.log("Start mail validation."); }

    let mailfield = document.getElementById('email-1');
    let mail = mailfield.value.trim();
    let invalidMailWarning = document.getElementById('email-1-label');

    if ( mail === '' || !formValidation.isValidEmail(mail)) {
      //invalidMailWarning.setAttribute("role", "alert");
      mailfield.setAttribute("aria-invalid", "true");
      mailfield.setAttribute("aria-describedby", "email-1-label"); // not great to hard-code all this stuff, but it's just a demo
    } else {
      //invalidMailWarning.removeAttribute("role");
      mailfield.removeAttribute("aria-invalid");
      mailfield.removeAttribute("aria-describedby");
    }

  },

  handleDateValidation:function() {
    "use strict";
    let startDateField = document.getElementById("startdate-1");
    let endDateField = document.getElementById("enddate-1");
    let startDate = startDateField.value;
    let endDate = endDateField.value;
    let invalidDateWarningBefore = document.getElementById('startdate-1-label');
    let invalidDateWarningAfter = document.getElementById('enddate-1-label');

    if ( !formValidation.startDateIsBeforeEndDate(startDate, endDate)) {
      if (startDate != "") {
        startDateField.setAttribute("aria-invalid", "true");
        invalidDateWarningBefore.setAttribute("role", "alert");
        startDateField.setAttribute("aria-describedby", "startdate-1-label");
      }
      if ( endDate != "") {
        endDateField.setAttribute("aria-invalid", "true");
        invalidDateWarningAfter.setAttribute("role", "alert");
        endDateField.setAttribute("aria-describedby", "enddate-1-label");      
      }
    } else if (startDate == "") {
      startDateField.setAttribute("aria-invalid", "true");
      invalidDateWarningBefore.setAttribute("role", "alert");
      startDateField.setAttribute("aria-describedby", "startdate-1-label");
    } else if ( endDate == "") {
      endDateField.setAttribute("aria-invalid", "true");
      invalidDateWarningAfter.setAttribute("role", "alert");
      endDateField.setAttribute("aria-describedby", "enddate-1-label");      
    } else {
      startDateField.removeAttribute("aria-invalid");
      endDateField.removeAttribute("aria-invalid");
      invalidDateWarningBefore.removeAttribute("role");
      invalidDateWarningAfter.removeAttribute("role");
      startDateField.removeAttribute("aria-describedby");
      endDateField.removeAttribute("aria-describedby");      
    }
    if (DEBUG) { console.log("Date validation has run."); }
  },

  startDateIsBeforeEndDate:function(startDate, endDate) {
    "use strict";
    let datesOK = false;
    if ( startDate < endDate) {
      datesOK = true;
    }
    return datesOK; 
  },

  isValidEmail:function(address) {
    "use strict";
    let isValid = false;
    // regex from https://stackoverflow.com/a/9204568/6002174 - only checks syntax, not whether the address exists, obviously
    let rex = /\S+@\S+\.\S+/;
    isValid = rex.test(address);
    return isValid;
  }
};

formValidation.init();
