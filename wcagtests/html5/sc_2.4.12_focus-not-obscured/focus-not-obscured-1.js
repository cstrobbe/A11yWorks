/*focus-not-obscured-1.js*/
/*jslint
    this
    for
*/

const DEBUG = false;

/**
 * @namespace The main application object.
 */
let testpage = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addUserNameListener();
    this.addPasswordListener();
    this.addClearFormListener();
    this.addOpacityToggleListener();
  },

  addUserNameListener:function() {
    "use strict";
    addListener(document.getElementById("username"), "blur", testpage.checkUserName);
  },

  checkUserName:function(event) {
    "use strict";
    // @@TODO replace with DOMhelp.stopDefault(event) and DOMhelp.stopBubble(event) (?)
    event.preventDefault();
    event.stopPropagation();
    //if (DEBUG) console.log("Triggered username blur");
    testpage.displayUserNameAlert(event.target, event.type);
  },

  displayUserNameAlert:function(target, type) {
    "use strict";
    let usernameAlert = document.getElementById("username-status");
    let username = target.value;
    if (DEBUG) { console.log("Username = " + username + ". Length = " + username.length); }
    if (username.length < 2) {
      usernameAlert.innerHTML = "Enter a username that is at least two characters long!";
      usernameAlert.classList.remove("hidden");
      usernameAlert.focus();
      //usernameAlert.setAttribute("tabindex", "0");
    }
  },

  addPasswordListener:function() {
    "use strict";
    addListener(document.getElementById("password"), "blur", testpage.checkPassword);
  },

  checkPassword:function(event) {
    "use strict";
    // @@TODO replace with DOMhelp.stopDefault(event) and DOMhelp.stopBubble(event) (?)
    event.preventDefault();
    event.stopPropagation();
    //if (DEBUG) console.log("Triggered password blur");
    testpage.displayPasswordAlert(event.target, event.type);
  },

  displayPasswordAlert:function(target, type) {
    "use strict";
    let passwordAlert = document.getElementById("password-status");
    let password = target.value;
    if (DEBUG) { console.log("Password = " + password + ". Length = " + password.length); }
    if (password.length < 1) {
      passwordAlert.classList.remove("hidden");
      passwordAlert.focus();
    }
  },

  addClearFormListener:function() {
    "use strict";
    //Don't prevent default or event propagation

    addListener(document.getElementById("login-form"), "reset", testpage.clearForm);
  },

  clearForm:function(event) {
    "use strict";
    //if (DEBUG) console.log("Triggered clear form");
    let usernameAlert = document.getElementById("username-status");
    usernameAlert.classList.add("hidden");
    let passwordAlert = document.getElementById("password-status");
    passwordAlert.classList.add("hidden");
    document.getElementById("form-instructions").focus();
  },

  addOpacityToggleListener:function() {
    "use strict";
    addListener(document.getElementById("opacitytoggle"), "click", testpage.toggleOpacity);
    //keypress listener redundant with click listener:
    //addListener(document.getElementById("opacitytoggle"), "keypress", testpage.toggleOpacity);
  },

  toggleOpacity:function() {
    "use strict";
    // @@TODO replace with DOMhelp.stopDefault(event) and DOMhelp.stopBubble(event) (?)
    event.preventDefault();
    event.stopPropagation();

    let obscurer = document.getElementById("obscurantism");
    let isNonTransparent = obscurer.classList.contains("nontransparent");
    if (DEBUG) console.log("In toggleOpacity. Status: " + isNonTransparent);

    if (isNonTransparent) {
      obscurer.classList.remove("nontransparent");
    } else {
      obscurer.classList.add("nontransparent");
    }
  }
};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    testpage.init();
});

