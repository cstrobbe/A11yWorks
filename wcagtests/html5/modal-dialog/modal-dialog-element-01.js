/*modal-dialog-element-01.js*/
/*jslint
    this
    for
*/

let DEBUG = false;
let USE_ARIA_MODAL = false;
const KEYCODE = {
  ESC: 27
};

/**
 * @namespace The main application object.
 */
let modalDialog = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addCookieButtonListener();
  },

  dialog: document.querySelector('.dialog'),
  previousActiveElement: null,

  checkCloseDialog:function(e) {
    "use strict";
    if (e.keyCode === KEYCODE.ESC) {
      modalDialog.closeDialog();
    }
  },

  addCookieButtonListener:function() {
    "use strict";
    document.getElementById('cookie-opener').addEventListener('click', modalDialog.handleCookieConsentRequest);
  },

  handleCookieConsentRequest:function() {
    "use strict";
    let theButton = event.target;
    let theDialog = document.getElementById(theButton.getAttribute("data-target"));
    modalDialog.openDialog(theDialog);
  },

  openDialog:function(aDialog) {
    "use strict";

    // Make dialog visible
    if (aDialog) {
      if (USE_ARIA_MODAL) aDialog.setAttribute("aria-modal", "true");
      aDialog.showModal();
    }

    // Listen for events that should close the dialog
    aDialog.querySelectorAll('button').forEach(btn => {
      btn.addEventListener('click', modalDialog.closeDialog);
    });
    document.addEventListener('keydown', modalDialog.checkCloseDialog);

    // Move focus into the dialog
    aDialog.querySelector('button').focus();
  },

  closeDialog:function() {
    "use strict";
    modalDialog.dialog.querySelectorAll('button').forEach(btn => {
      btn.removeEventListener('click', modalDialog.closeDialog);
    });
    document.removeEventListener('click', modalDialog.closeDialog);

    if (USE_ARIA_MODAL) modalDialog.dialog.removeAttribute("aria-modal");

    // Close the dialog window
    modalDialog.dialog.close();
  }

};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    modalDialog.init();
});

/**
 * Add a function to the list of functions that need to be called when
 * the HTML document has finished loading (in other words, after the <code>window.load</code> event).
 * @param {Function} func A function that needs to be invoked after <code>window.load</code>.
 * @static
 * @author Simon Willison
 * @see Simon Willison's article <a href="https://simonwillison.net/2004/May/26/addLoadEvent/">Executing JavaScript on page load</a> (26 May 2004; last acccessed on 09.08.2022).
 */
function addLoadEvent(func) {
    "use strict";
    var oldonload = window.onload;
    if (typeof window.onload !== 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        };
    }
}
