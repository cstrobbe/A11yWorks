/*modal-dialog-01-javascript-trap.js*/
/*jslint
    this
    for
*/

let DEBUG = false;
const KEYCODE = {
  ESC: 27
};

/**
 * @namespace The main application object.
 */
let modalDialog = {
  init: function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) { return; }

    this.addCookieButtonListener();
    this.addTabEventListener();
  },

  dialog: document.querySelector('.dialog'),
  dialogMask: document.querySelector('.dialog__mask'),
  dialogWindow: document.querySelector('.dialog__window'),// instead of dialog.querySelector('.dialog__window')
  previousActiveElement: null,

  checkCloseDialog:function(e) {
    "use strict";
    if (e.keyCode === KEYCODE.ESC) {
      modalDialog.closeDialog();
    }
  },

  addCookieButtonListener:function() {
    "use strict";
    document.getElementById('cookie-opener').addEventListener('click', modalDialog.handleCookieConsentRequest);
  },

  // Code adapted from Hidde de Vries: https://hidde.blog/using-javascript-to-trap-focus-in-an-element/
  addTabEventListener:function() {
    "use strict";
    var focusableEls = modalDialog.dialog.querySelectorAll('a[href]:not([disabled]), button:not([disabled]), textarea:not([disabled]), input[type="text"]:not([disabled]), input[type="radio"]:not([disabled]), input[type="checkbox"]:not([disabled]), select:not([disabled]), iframe, summary, area[href]');
    var firstFocusableEl = focusableEls[0];
    var lastFocusableEl = focusableEls[focusableEls.length - 1];
    var KEYCODE_TAB = 9;
    modalDialog.dialog.addEventListener('keydown', function(e) {
      var isTabPressed = (e.key === 'Tab' || e.keyCode === KEYCODE_TAB);
  
      if (!isTabPressed) { 
        return; 
      }
  
      if ( e.shiftKey ) /* shift + tab */ {
        if (document.activeElement === firstFocusableEl) {
          lastFocusableEl.focus();
            e.preventDefault();
          }
        } else /* tab */ {
        if (document.activeElement === lastFocusableEl) {
          firstFocusableEl.focus();
            e.preventDefault();
          }
        }
    });
  },

  handleCookieConsentRequest:function() {
    "use strict";
    modalDialog.openDialog();
  },

  openDialog:function() {
    "use strict";
    // Store reference to previously active element
    modalDialog.previousActiveElement = document.ActiveElement;

    if (modalDialog.previousActiveElement === null || modalDialog.previousActiveElement === undefined) {
      modalDialog.previousActiveElement = document.getElementById('cookie-opener');
    }

    // Make dialog visible
    modalDialog.dialog.classList.add('opened');

    // Listen for events that should close the dialog
    modalDialog.dialogMask.addEventListener('click', modalDialog.closeDialog);
    modalDialog.dialogWindow.querySelectorAll('button').forEach(btn => {
      btn.addEventListener('click', modalDialog.closeDialog);
    });
    document.addEventListener('keydown', modalDialog.checkCloseDialog);

    // Move focus into the dialog
    modalDialog.dialog.querySelector('button').focus();
  },

  closeDialog:function() {
    "use strict";
    modalDialog.dialogMask.removeEventListener('click', modalDialog.closeDialog);
    modalDialog.dialogWindow.querySelectorAll('button').forEach(btn => {
      btn.removeEventListener('click', modalDialog.closeDialog);
    });
    document.removeEventListener('click', modalDialog.closeDialog);

    // Hide the dialog window
    modalDialog.dialog.classList.remove('opened');

    // Restore focus to previously active element
    // Without this, focus moves back to the top of the page
    modalDialog.previousActiveElement.focus({focusVisible: true});
  }

};


addLoadEvent(function() {
    "use strict";
    // Check support for W3C DOM. (C. Heilmann: Beginning JavaScript Development ..., p. 66-68.)
    if (!document.getElementById || !document.createTextNode) {return;}
    modalDialog.init();
});

/**
 * Add a function to the list of functions that need to be called when
 * the HTML document has finished loading (in other words, after the <code>window.load</code> event).
 * @param {Function} func A function that needs to be invoked after <code>window.load</code>.
 * @static
 * @author Simon Willison
 * @see Simon Willison's article <a href="https://simonwillison.net/2004/May/26/addLoadEvent/">Executing JavaScript on page load</a> (26 May 2004; last acccessed on 09.08.2022).
 */
function addLoadEvent(func) {
    "use strict";
    var oldonload = window.onload;
    if (typeof window.onload !== 'function') {
        window.onload = func;
    } else {
        window.onload = function() {
            if (oldonload) {
                oldonload();
            }
            func();
        };
    }
}

